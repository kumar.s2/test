package org.example;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import java.net.URL;

import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

public class BstackNWlogsTest {

    public WebDriver driver;
    public Properties prop; //creating Properties object
    String[] itemsNeeded = {"Cucumber", "Brocolli", "Brinjal"};
    public static final String AUTOMATE_USERNAME = "kumarshubham_XIv1zY";
    public static final String AUTOMATE_ACCESS_KEY = "k11H2xQkzswdQf3Zz6qe";
    public static final String URL = "https://" + AUTOMATE_USERNAME + ":" + AUTOMATE_ACCESS_KEY + "@hub-cloud.browserstack.com/wd/hub";

    @BeforeMethod
    public void setup() throws IOException {
/*        prop=new Properties();
        FileInputStream fis = new FileInputStream("");
        prop.load(fis);
        prop.getProperty("");*/
        DesiredCapabilities caps = new DesiredCapabilities();
        HashMap networkLogsOptions = new HashMap<>();
        networkLogsOptions.put("captureContent", Boolean.TRUE);
        caps.setCapability("browserstack.networkLogs", "true");
        caps.setCapability("browserstack.networkLogsOptions", networkLogsOptions);
        caps.setCapability("os", "Windows");
        caps.setCapability("os_version", "10");
        caps.setCapability("browser", "Chrome");
        caps.setCapability("browser_version", "latest");
        caps.setCapability("build", "N/w Logs debug");
        driver = new RemoteWebDriver(new URL(URL), caps);
        driver.get("https://rahulshettyacademy.com/seleniumPractise/#/");
        driver.manage().window().maximize();
        driver.manage().deleteAllCookies();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
    }

    @Test
    public void firstTest() throws InterruptedException, IOException {

        cart(driver, itemsNeeded);
        driver.findElement(By.xpath("//a[@class='cart-icon']/img")).click();
        Thread.sleep(2000);
        driver.findElement(By.xpath("//button[text()='PROCEED TO CHECKOUT']")).click();

        WebElement totalAmount = driver.findElement(By.xpath("//span[@class='discountAmt']"));
        Explicitwait(driver,totalAmount);
        System.out.println(totalAmount.getText());

        driver.findElement(By.xpath("//button[text()='Place Order']")).click();

        WebElement countries = driver.findElement(By.xpath("//body/div[@id='root']/div[1]/div[1]/div[1]/div[1]/div[1]/select[1]"));

        Explicitwait(driver,countries);
        Select options = new Select(countries);
        options.selectByValue("India");

        driver.findElement(By.xpath("//input[@class='chkAgree']")).click();
        driver.findElement(By.xpath("//button[text()='Proceed']")).click();

        WebElement search = driver.findElement(By.xpath("//input[@class='search-keyword']"));
        Explicitwait(driver,search);

/*        TakesScreenshot ts = (TakesScreenshot) driver;
        File src=ts.getScreenshotAs(OutputType.FILE);
        FileUtils.copyFile(src,new File("C://"));*/




    }

    @AfterMethod(alwaysRun = true)
    public void tearDown() {

        driver.quit();
    }

/*    @DataProvider(name = "search")
    public Object[][] getData() {

        return new Object[][]{
                {"Shubham", "India"},
                {"Manya", "USA"}

        };
    }


    @Test(dataProvider = "search")
    public void test2(String name, String country) {
        System.out.println(name + " belongs to " + country);
    }*/

    public void cart(WebDriver driver, String[] itemsNeeded) throws InterruptedException {

        int j = 0;
        List<WebElement> products = driver.findElements(By.xpath("//h4[@class='product-name']"));

        for (int i = 0; i < products.size(); i++) {

            String[] name = products.get(i).getText().split("-");
            String formatedName = name[0].trim();

            List item = Arrays.asList(itemsNeeded);

            if (item.contains(formatedName)) {

                j++;
                driver.findElements(By.xpath("//div[@class='product-action']/button")).get(i).click();
                Thread.sleep(2000);

                if (j == itemsNeeded.length) {
                    break;
                }

            }
        }
    }

    public void Explicitwait(WebDriver driver,WebElement element){

        WebDriverWait w= new WebDriverWait(driver,10);
        w.until(ExpectedConditions.visibilityOf(element));
    }

}
