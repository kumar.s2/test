package org.example;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.reporter.ExtentSparkReporter;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.*;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

public class Demo1 {

    public WebDriver driver;
    public Properties prop; //creating Properties object
    public ExtentReports extent;
    String[] itemsNeeded = {"Cucumber", "Brocolli", "Brinjal"};

    @BeforeTest
    public void cofig(){

        String path =System.getProperty("user.dir")+"\\reports\\Shubham.html";
        ExtentSparkReporter reporter = new ExtentSparkReporter(path);
        reporter.config().setReportName("Web Automation test report");
        reporter.config().setDocumentTitle("Demo Test");

        extent=new ExtentReports();
        extent.attachReporter(reporter);
        extent.setSystemInfo("Tester","SHUBHAM");

    }

    @BeforeMethod
    public void setup() throws IOException {
/*        prop=new Properties();
        FileInputStream fis = new FileInputStream("");
        prop.load(fis);
        prop.getProperty("");*/
        System.setProperty("webdriver.chrome.driver", "/Users/kumarshubham/Downloads/chromedriver");
        driver = new ChromeDriver();

        driver.get("https://rahulshettyacademy.com/seleniumPractise/#/");
        driver.manage().window().maximize();
        driver.manage().deleteAllCookies();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
    }

    @Test
    public void firstTest() throws InterruptedException, IOException {

        extent.createTest("Greenkart");
        cart(driver, itemsNeeded);
        driver.findElement(By.xpath("//a[@class='cart-icon']/img")).click();
        Thread.sleep(2000);
        driver.findElement(By.xpath("//button[text()='PROCEED TO CHECKOUT']")).click();

        WebElement totalAmount = driver.findElement(By.xpath("//span[@class='discountAmt']"));
        Explicitwait(driver, totalAmount);
        System.out.println(totalAmount.getText());

        driver.findElement(By.xpath("//button[text()='Place Order']")).click();

        WebElement countries = driver.findElement(By.xpath("//body/div[@id='root']/div[1]/div[1]/div[1]/div[1]/div[1]/select[1]"));

        Explicitwait(driver, countries);
        Select options = new Select(countries);
        options.selectByValue("India");

        driver.findElement(By.xpath("//input[@class='chkAgree']")).click();
        driver.findElement(By.xpath("//button[text()='Proceed']")).click();

        WebElement search = driver.findElement(By.xpath("//input[@class='search-keyword']"));
        Explicitwait(driver, search);

/*        TakesScreenshot ts = (TakesScreenshot) driver;
        File src=ts.getScreenshotAs(OutputType.FILE);
        FileUtils.copyFile(src,new File("C://"));*/

        extent.flush();

    }

    @AfterMethod(alwaysRun = true)
    public void tearDown() {

        driver.close();
    }

/*    @DataProvider(name = "search")
    public Object[][] getData() {

        return new Object[][]{
                {"Shubham", "India"},
                {"Manya", "USA"}

        };
    }


    @Test(dataProvider = "search")
    public void test2(String name, String country) {
        System.out.println(name + " belongs to " + country);
    }*/

    public void cart(WebDriver driver, String[] itemsNeeded) throws InterruptedException {

        int j = 0;
        List<WebElement> products = driver.findElements(By.xpath("//h4[@class='product-name']"));

        for (int i = 0; i < products.size(); i++) {

            String[] name = products.get(i).getText().split("-");
            String formatedName = name[0].trim();

            List item = Arrays.asList(itemsNeeded);

            if (item.contains(formatedName)) {

                j++;
                driver.findElements(By.xpath("//div[@class='product-action']/button")).get(i).click();
                Thread.sleep(2000);

                if (j == itemsNeeded.length) {
                    break;
                }

            }
        }
    }

    public void Explicitwait(WebDriver driver, WebElement element) {

        WebDriverWait w = new WebDriverWait(driver, 10);
        w.until(ExpectedConditions.visibilityOf(element));
    }

}
