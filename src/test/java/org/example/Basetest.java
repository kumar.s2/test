package org.example;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;

import java.io.IOException;
import java.net.URL;
import java.util.HashMap;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

public class Basetest {


    public WebDriver driver;
    public WebDriverWait w;
    public Actions a;
    public Properties prop; //creating Properties object
    public static final String AUTOMATE_USERNAME = "kumarshubham_XIv1zY";
    public static final String AUTOMATE_ACCESS_KEY = "k11H2xQkzswdQf3Zz6qe";
    public static final String URL = "https://" + AUTOMATE_USERNAME + ":" + AUTOMATE_ACCESS_KEY + "@hub-cloud.browserstack.com/wd/hub";

    @BeforeMethod
    public void setup() throws IOException {
/*        prop=new Properties();
        FileInputStream fis = new FileInputStream("");
        prop.load(fis);
        prop.getProperty("");*/
        DesiredCapabilities caps = new DesiredCapabilities();
        HashMap networkLogsOptions = new HashMap<>();
        networkLogsOptions.put("captureContent", Boolean.TRUE);
        caps.setCapability("browserstack.networkLogs", "true");
        caps.setCapability("browserstack.networkLogsOptions", networkLogsOptions);
        caps.setCapability("os", "Windows");
        caps.setCapability("os_version", "10");
        caps.setCapability("browser", "Chrome");
        caps.setCapability("browser_version", "latest");
        caps.setCapability("browserstack.debug", "true");
        // caps.setCapability("browserstack.video", "false");
        caps.setCapability("build", "N/w Logs debug");
        driver = new RemoteWebDriver(new URL(URL), caps);
        driver.manage().window().maximize();
        driver.manage().deleteAllCookies();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
       /* System.setProperty("webdriver.chrome.driver", "/Users/kumarshubham/Downloads/chromedriver");
        driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.manage().deleteAllCookies();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);*/
    }


    @AfterMethod(alwaysRun = true)
    public void tearDown() {

        driver.quit();
    }

    public void Explicitwait(WebDriver driver, WebElement element) {

        w = new WebDriverWait(driver, 10);
        w.until(ExpectedConditions.visibilityOf(element));
    }

}
